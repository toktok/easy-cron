const cronDesc = (cron) => {
  if (cron) {
    let cronParts = cron.split(' ');
    let minIdx = 0;
    let sec;
    let text = '';
    //cron表达式一般由5个或者6个空格间隔，组成6个或者7个域
    //e.g. * 16 8 * * ? 每日08时16分
    if (cronParts.length > 5) {
      sec = cronParts[0];
      minIdx = 1;
    }
    if (sec) {
      if (sec != '*'){
        text = sec + '秒';
      }
    }
    let minutes = cronParts[minIdx]
    if (minutes === '*') {
      if (text !== '') {
        text = "每分" + text;
      }
    } else {
      text = minutes + "分" + text;
    }
    let hours = cronParts[minIdx + 1]
    if (hours === '*') {
      if (text !== '') {
        text = "每时" + text;
      }
    } else {
      text = hours + "时" + text;
    }
    let dat = cronParts[minIdx + 2]
    if (dat !== '?') {
      if (dat === '*') {
        text = '每日' + text
      } else if (dat === 'L') {
        text = '末日' + text
      } else {
        text = '每' + dat + '日' + text
      }
    }
    let year = cronParts[minIdx + 5]
    let month = cronParts[minIdx + 3]
    if (month === '*') {
      if (year && year !== '*') {
        text = "每月" + text;
      }
    } else {
      text = month + "月" + text;
    }
    let week = cronParts[minIdx + 4]
    if (week !== '?') {
      if (week === '*') {
        //text = '每日'
      } else if (week.endsWith('L')) {
        text = '最后周' + week.substring(0, week.length - 1)  + text
      } else {
        text = '每周' + week + text
      }
    }
    if (year && year !== '*') {
      text = year + "年" + text;
    }
    return text;
  }
  return '';
}

export default cronDesc
